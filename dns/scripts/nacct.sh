#!/bin/bash

NEWNAME=/var/log/nacct/net-acct.`date +'%d-%H-%M'`
if [ ! -f /var/log/net-acct ] ; then
  echo "Too early"
  exit
fi
mv /var/log/net-acct $NEWNAME
cat $NEWNAME|/home/scripts/parse_nacct.pl


