#!/usr/bin/perl

my $ipt_recent_file = '/proc/net/ipt_recent/dns_blacklist';
my $log_blocked = "/var/log/dns_blocked.hosts";

sub logit {
  my ( $msg )  = @_; 
  my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
  $year = $year + 1900;
  $mon += 1;
  open(my $log, '>>', $log_blocked) or die "Could not open file '$ipt_recent_file' $!";
  print $log "$mday/$mon/$year $hour:$min $msg \n";
  close $log;
}

while(<STDIN>)
{
 $sLine = $_;
 chomp;
 @aLine = split (/\s+/);
 $proto = $aLine[1];
 $src = $aLine[2];
 $srcport = $aLine[3];
 $dst = $aLine[4];
 $dstport = $aLine[5];
 $pkts = $aLine[6];
 $bytes = $aLine[7];

 next if $proto != 17;
 next if $src =~ /^89.249.2/ and  $dst =~ /^89.249.2/;
 next if $srcport != 53 and $dstport != 53;
 $chain='INPUT' if $dst =~ /89.249.2/;
 $chain='OUTPUT' if $src =~ /89.249.2/;
 if ( $chain eq 'INPUT')
 {
  next if $dstport != 53;
  $hInput{$src}{packets}+=$pkts;
  $hInput{$src}{bytes}+=$bytes;
 }

 if ( $chain eq 'OUTPUT')
 {
  next if $srcport != 53;
  $hOutput{$dst}{packets}+=$pkts;
  $hOutput{$dst}{bytes}+=$bytes;
 }
}

#print "INPUT traffic to 53 port (ip pkts bytes)\n";
# $i=0;
# foreach $key (sort {$hInput{$b}{packets}<=>$hInput{$a}{packets}} keys(%hInput))
# {
#   print $key." ".$hInput{$key}{packets}." ".$hInput{$key}{bytes}."\n";
#   last if $i++==9;
# }
print "OUTPUT traffic to 53 port (ip pkts bytes)\n";
 $i=0;
 foreach $key (sort {$hOutput{$b}{packets}<=>$hOutput{$a}{packets}} keys(%hOutput))
 {
   open(my $fh, '>>', $ipt_recent_file) or die "Could not open file '$ipt_recent_file' $!";
   logit( "$key $hOutput{$key}{packets} $hOutput{$key}{bytes}" ) if $hOutput{$key}{bytes}>1024*1024;
   print $fh $key if $hOutput{$key}{bytes}>1024*1024;
   close $fh;
   #last if $i++==9;
 }
